<?php

class Database
{
    private $connection;

    public function __construct($host, $user, $password, $schema) {

        $this->connection = new mysqli($host, $user, $password, $schema);

        if ($this->connection->connect_error) {

            die("Database connection failed: " . $this->connection->connect_error);
        }
    }

    public function __destruct() {

        $this->connection->close();
    }

    public function runQuery($sql) {

        $result = $this->connection->query($sql);

        if (!$result) {

            echo $this->connection->error;
        }

        if ($result->num_rows > 0) {

            while ($row = $result->fetch_assoc()) {

                $resultset[] = $row;
            }
        }

        if (!empty($resultset)) {

            return $resultset;
        }

        return "Query executed successfully\n";
    }
}
