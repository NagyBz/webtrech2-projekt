var octaveIsRunning = false;

$( document ).ready(function() {
    writeWelcomeText();

    $('#input-command').keyup(function(e){
        if(e.which == 13) {

            let command = $('#input-command').val();

            switch(command) {
                case "help":
                    command = help();
                    break;
                case "clear":
                    command = clear();
                    break;
                case "home":
                    command = home();
                    break;
                case "start":
                    command = start();
                    break;
                case "exit":
                    command = exit();
                    break;
                default:
                    command = "";
            }

            // logCommand();

            if (command !== "")
            {
                clearCommandList($('#input-command').val());
                typeWriter(0, command, 50, "welcomeText");
                showCommandLine(50, command.length);
            }
            else if(octaveIsRunning && $('#input-command').val() !== "start")
            {
                executeOctaveCommand();
            }
            else
            {
                if(!octaveIsRunning)
                {
                    $('#div-log-commands').empty();
                    typeWriter(0, "Command not found!", 50, "div-log-commands");
                }

            }
            $('#input-command').val("");


        }
    });

});

function clearCommandList(command)
{
    $('#welcomeText').empty();
    $('#div-log-commands').empty();
    $('#reply').empty();
    $('#input-command').val("");
    $('#div-octave-input').css('display','none');
    if (command !== "help")
    {
        $('#ul-help').css('display', 'none');
    }

}

function logCommand() {
    let val = $('#input-command').val();
    let log = $("<p id='p-logged-command' class='text-secondary'><span id='span-octave'>cli:~$ </span> " + val + "</p>")
    $('#input-command').val("");
    $('#div-log-commands').append(log);
}

function writeWelcomeText()
{
    let speed = 50;
    let text = "Welcome to Octave CLI - type your code then press ENTER. For more information type 'help'...";

    typeWriter(0, text, speed, "welcomeText");
    showCommandLine(speed, text.length);
}

function showCommandLine(speed, length)
{
    setTimeout(function () {
        $('#div-octave-input').css('display','block');
    }, speed * length);
}

function typeWriter(i, txt, speed, elementId) {
    if (i < txt.length) {
        document.getElementById(elementId).innerHTML += txt.charAt(i);
        i++;
        setTimeout(function(){
            typeWriter(i++, txt, speed, elementId);
        }, speed);
    }
}


function help() {
    createHelpField();
    return "Do you need help?";
}

function clear() {
    return 'All clear!';
}

function home() {
    return "Welcome to Octave CLI - type your code then press ENTER. For more information type 'help'...";
}

function start() {
    startCas('../zoli/start.m');
    octaveIsRunning = true;
    return "Octave is running now";
}

function exit()
{
    octaveIsRunning = false;
    return "Octave has been terminated";
}

function startCas(file) {

    // sent request for starting CAS
    $.ajax({
        method: "POST",
        url: "pendulum/cas-start.php",
        data: { "file": file },
        error: function (response) {
            alert(response.length);
        }
    });
}

function isValidJson(str) {

    try {

        JSON.parse(str);

    } catch (e) {
        return false;
    }
    return true;
}

function executeOctaveCommand()
{
    console.log("command sent");
    let cmd = $("#input-command").val();

    $("#reply").load("../zoli/zoli-cas.php", {

        command: cmd

    }, function(response, status) {

        console.log("Response: " + response);
        console.log("Status: " + status);
        console.log("Valid JSON: " + isValidJson(response));

        if (isValidJson(response)) {
            console.log(response);
        }

    });
}

function createHelpField() {
    $('#ul-help').css('display', 'block');

    $('#li-home').empty();
    $('#li-start').empty();
    $('#li-exit').empty();
    $('#li-help').empty();
    $('#li-clear').empty();

    let help_text = "'help' - The list of available commands, which are supported in this CLI.";
    let start_text = "'start' - Execute Octave CLI. Without this command you are not able to run Octave commands.";
    let exit_text = "'exit' - Close Octave CLI.";
    let home_text = "'home' - Return to the home screen.";
    let clear_text = "'clear' - Clear the list of executed commands.";

    typeWriter(0, home_text, 30, "li-home");
    typeWriter(0, help_text, 30, "li-help");
    typeWriter(0, clear_text, 30, "li-clear");
    typeWriter(0, start_text, 30, "li-start");
    typeWriter(0, exit_text, 30, "li-exit");
}


