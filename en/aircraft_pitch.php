<?php
include_once "../zoli/frequency-config.php";


include "../log_request.php";
log_page("task2");


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Aircraft Pitch</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="index.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="../language.js" type="application/javascript"></script>
    <script src="aircraft_pitch.js" type="application/javascript"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="nav-menubar">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individual Tasks
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="inverted_pendulum.php">Inverted Pendulum</a>
                    <a class="dropdown-item" href="suspention/suspension.php">Suspension</a>
                    <a class="dropdown-item" href="ball_and_beam.php">Ball & Beam</a>
                    <a class="dropdown-item" href="aircraft_pitch.php">Aircraft Pitch</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Information
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="statistics.php">Statistics</a>
                    <a class="dropdown-item" href="documentation.php">API Documentation</a>
                    <a class="dropdown-item" href="tasks.php">Task Management</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Language
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">English</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovak</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <div class="col-xs-10 mx-auto" id="div-animation">
                <svg id="svg-svg"
                     width="666.66669" height="466.66669"
                     xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <!-- Created with SVG-edit - http://svg-edit.googlecode.com/ -->
                    <g>
                        <rect fill="#7f7f7f" stroke="null" stroke-width="5" stroke-dasharray="null" x="292.34067" y="373.12499" width="3" height="0" id="svg_3"/>
                        <g id="svg_15"/>
                        <g id="svg_19"/>
                        <use id="svg_22" xlink:href="#svg_21" transform="matrix(0.7716049551963807,0,0,0.7716049551963807,12.750000000000007,11.3125) " y="0" x="0"/>
                        <g id="svg_23"/>
                    </g>
                    <g>
                        <g id="svg_9" fill="#60b8d1"/>
                        <g id="svg_14">
                            <path id="svg_1" d="m206,231a48,48 0 0 0 -48,48l-16,0a31.992,31.992 0 0 0 -31.992,32.735c0.392,17.491 15.136,31.265 32.631,31.265l300.03,0a96,96 0 0 0 85.865,-53.068l13.466,-26.932l0,-32l-336,0z" fill="#dadcde"/>
                            <path id="svg_2" d="m222,231l0,48l-64,0a48,48 0 0 1 48,-48l16,0z" fill="#b6b9ba"/>
                            <path id="svg_4" d="m514.23,311a96.006,96.006 0 0 1 -71.56,32l-300.03,0c-17.5,0 -32.24,-13.77 -32.63,-31.27c-0.01,-0.24 -0.01,-0.49 -0.01,-0.73l404.23,0z" fill="#67c5e0"/>
                            <path id="svg_5" d="m574,151l-48,0l-64,80l80,0l32,-80z" fill="#3397e8"/>
                            <path id="svg_7" d="m374,311l-104,0a16,16 0 0 1 -16,-16a16,16 0 0 1 16,-16l104,0l0,32z" fill="#b6b9ba"/>
                            <path id="svg_8" d="m414,263l-48,0l-64,48l80,0l32,-48z" fill="#3397e8"/>
                            <path id="svg_10" d="m246,255l16,0l0,16l-16,0l0,-16z"/>
                            <path id="svg_11" d="m278,255l16,0l0,16l-16,0l0,-16z"/>
                            <path id="svg_12" d="m310,255l16,0l0,16l-16,0l0,-16z"/>
                            <path id="svg_13" d="m303.985,352.752l-49.985,6.248l0,-48l49.985,6.248a16,16 0 0 1 14.015,15.876l0,3.752a16,16 0 0 1 -14.015,15.876z" fill="#3397e8"/>
                        </g>
                        <path id="svg_6" d="m574,231l-48,0l-64,32l80,0l32,-32z" fill="#3397e8" transform="rotate(-2.1213488578796387 517.9999999999984,246.99999999999955) "/>
                    </g>
                </svg>
            </div>
        </div>
        <div class="col-md-7" id="div-graph">
            <div class="col-xs-12 mx-auto" id="div-graph-result" style="height: 600px; width: 100%;"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 mx-auto" >
            <div class="col-xs-12 mx-auto d-flex flex-row">
                <label for="input-slider">Range: </label>
                <p id="p-slide-range">0.5</p>
                <input type="range" class="form-control-range" id="input-slider" min="0.3" max="1" step="0.05" value="0.5" oninput="rangeSliderWatcher(this.value)">
                <button class="btn btn-primary m-3 " onclick="getData(<?php echo ANIMATION_FREQUENCY; ?>, this)">Draw</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 mx-auto" >
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="checkbox-graph" checked>
                <label class="form-check-label" for="checkbox-graph">Show graph</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="checkbox-animation"  checked>
                <label class="form-check-label" for="checkbox-animation">Show animation</label>
            </div>
        </div>
    </div>
</div>
</body>
</html>