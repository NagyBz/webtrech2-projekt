<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>API Dokumentácia</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="index.css" type="text/css">
    <link rel="stylesheet" href="../api-doc-styles/api-doc.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/gojs/release/go-debug.js"></script>
    <script src="../language.js"></script>
    <script src="../api-doc-styles/api-doc.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individual Tasks
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="inverted_pendulum.php">Inverted Pendulum</a>
                    <a class="dropdown-item" href="suspention/suspension.php">Suspension</a>
                    <a class="dropdown-item" href="ball_and_beam.php">Ball & Beam</a>
                    <a class="dropdown-item" href="aircraft_pitch.php">Aircraft Pitch</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Information
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="statistics.php">Statistics</a>
                    <a class="dropdown-item" href="documentation.php">API Documentation</a>
                    <a class="dropdown-item" href="tasks.php">Task Management</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Language
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">English</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovak</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div id="mySidenav" class="sidenav">

    <div class="closebtn" onclick="closeNav()">&times;</div>
    <div onclick="changeDoc('Class diagram')">Class&nbsp;diagram</div>
    <div onclick="changeDoc('Classes and methods')">Classes&nbsp;and&nbsp;methods</div>
    <div onclick="changeDoc('Endpoints')">Endpoints</div>

</div>

<div id="main">

    <span id="span-open-nav" onclick="openNav()">&#9776;</span>
    <h2 id="doc-title">Class diagram</h2>

</div>

<div id="div-diagram"></div>

<div id="div-table" >

    <h4>Description of methods</h4>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-cas" name="Cas" value="Cas" onchange="checkboxController('table-method', this)">
    <label for="checkbox-cas">Cas</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-cas-comm" name="CasCommunicator" value="CasCommunicator" onchange="checkboxController('table-method', this)">
    <label for="checkbox-cas-comm">CasCommunicator</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-command-logger" name="CommandLogger" value="CommandLogger" onchange="checkboxController('table-method', this)">
    <label for="checkbox-command-logger">CommandLogger</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-database" name="Database" value="Database" onchange="checkboxController('table-method', this)">
    <label for="checkbox-database">Database</label><br>

    <table id="table-method">
        <tr>
            <th onclick="sortTableString('table-method', 0)">Class</th>
            <th onclick="sortTableString('table-method', 1)">Method</th>
            <th onclick="sortTableString('table-method', 2)">Argument(s): type</th>
            <th onclick="sortTableString('table-method', 3)">Return value</th>
            <th onclick="sortTableString('table-method', 4)">Access modifier</th>
            <th onclick="sortTableString('table-method', 5)">Description</th>
        </tr>
        <tr>
            <td>CommandLogger</td>
            <td>logIntoDatabase</td>
            <td>commandLog: CommandLog</td>
            <td>Return value of called method<br>'runQuery' (ResultSet or string)</td>
            <td>public</td>
            <td>Method saves 'CommandLog' into database</td>
        </tr>
        <tr>
            <td>Database</td>
            <td>runQuery</td>
            <td>sql: string</td>
            <td>Result of query (ResultSet) in case of success;<br>
                error message as string in case of failure
            </td>
            <td>public</td>
            <td>Method is for running queries</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>connect</td>
            <td>host: string<br>port: int</td>
            <td>TRUE in case of success<br>FALSE in case of failure</td>
            <td>public</td>
            <td>Initialises socket connection with given address and port</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>disconnect</td>
            <td></td>
            <td>TRUE in case of success<br>FALSE in case of failure</td>
            <td>public</td>
            <td>Closes connection</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>send</td>
            <td>message: string</td>
            <td>Number of bytes written onto socket<br>FALSE in case of failure</td>
            <td>public</td>
            <td>Sends command (data) through socket</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>receive</td>
            <td>length: int</td>
            <td>Number of bytes received</td>
            <td>public</td>
            <td>Receives reply (data) through socket</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>contains</td>
            <td>haystack: string<br>needle: string</td>
            <td>TRUE, if 'haystack' contains substring 'needle'<br>FALSE otherwise</td>
            <td>private</td>
            <td>Checks, if 'needle' is substring of 'haystack'</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>start</td>
            <td>file: string<br>port: int</td>
            <td>Error message in case of failure</td>
            <td>public static</td>
            <td>Starts Octave and runs code in 'file'.<br>Parameter 'port' is passed as an argument to the program</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>stop</td>
            <td></td>
            <td></td>
            <td>public</td>
            <td>Send kill message to Octave</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>connect</td>
            <td>host: string<br>port: int</td>
            <td>Return value of 'CasCommunicator::connect'</td>
            <td>public</td>
            <td>Establishes connection with Octave.<br>Delegates method 'connect' of class 'CasCommunicator'</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>disconnect</td>
            <td></td>
            <td>Return value of 'CasCommunicator::disconnect'</td>
            <td>public</td>
            <td>Closes connection with Octave.<br>Delegates method 'disconnect' of class 'CasCommunicator'</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>exec</td>
            <td>command: string<br>logCommand: boolean</td>
            <td>Result of command execution</td>
            <td>public</td>
            <td>Sends command to Octave, which it then runs.<br>
                Returns information of error in case of failure.<br>
                If 'logCommand' is set on TRUE, logs command to database</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>log</td>
            <td>command: string<br>reply: string</td>
            <td>Return value of 'Logger::logIntoDatabase'</td>
            <td>private</td>
            <td>Logs information about the command<br>(command, correctness, error message)</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>preloadParametersForGraph</td>
            <td>file: string</td>
            <td>Return value of 'Cas::exec'</td>
            <td>public</td>
            <td>Runs commands from 'file' in Octave</td>
        </tr>
    </table>

    <br><br>

    <h4>Description of attributes</h4>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-cas-atr" name="Cas" value="Cas" onchange="checkboxController('table-field', this)">
    <label for="checkbox-cas-atr">Cas</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-cas-comm-atr" name="CasCommunicator" value="CasCommunicator" onchange="checkboxController('table-field', this)">
    <label for="checkbox-cas-comm-atr">CasCommunicator</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-command-logger-atr" name="CommandLogger" value="CommandLogger" onchange="checkboxController('table-field', this)">
    <label for="checkbox-command-logger-atr">CommandLogger</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-command-log-atr" name="CommandLog" value="CommandLog" onchange="checkboxController('table-field', this)">
    <label for="checkbox-command-log-atr">CommandLog</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-database-atr" name="Database" value="Database" onchange="checkboxController('table-field', this)">
    <label for="checkbox-database-atr">Database</label><br>

    <table id="table-field">
        <tr>
            <th onclick="sortTableString('table-field', 0)">Class</th>
            <th onclick="sortTableString('table-field', 1)">Attribute</th>
            <th onclick="sortTableString('table-field', 2)">Data type</th>
            <th onclick="sortTableString('table-field', 3)">Access modifier</th>
            <th onclick="sortTableString('table-field', 4)">Description</th>
        </tr>
        <tr>
            <td>CommandLog</td>
            <td>command</td>
            <td>string</td>
            <td>private</td>
            <td>Command from client CLI</td>
        </tr>
        <tr>
            <td>CommandLog</td>
            <td>commandCorrect</td>
            <td>boolean</td>
            <td>private</td>
            <td>Tells, whether the command was executed successfully or error occurred</td>
        </tr>
        <tr>
            <td>CommandLog</td>
            <td>errorMessage</td>
            <td>string</td>
            <td>private</td>
            <td>Error message from CAS</td>
        </tr>
        <tr>
            <td>CommandLogger</td>
            <td>database</td>
            <td>Database</td>
            <td>private</td>
            <td>Instance of class 'Database', which is used to insert command log into database</td>
        </tr>
        <tr>
            <td>Database</td>
            <td>connection</td>
            <td>mysqli_connection</td>
            <td>private</td>
            <td>Runs queries to communicate with database</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>socket</td>
            <td>PHP socket</td>
            <td>private</td>
            <td>Instance of PHP socket, which communicates with the other socket created in Octave</td>
        </tr>

        <tr>
            <td>Cas</td>
            <td>logger</td>
            <td>CommandLogger</td>
            <td>private</td>
            <td>Instance of class 'CommandLogger', which logs commands into database</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>communicator</td>
            <td>CasCommunicator</td>
            <td>private</td>
            <td>Instance of class 'CasCommunicator', which delegates methods of this class</td>
        </tr>
    </table>
</div>

<div id="div-endpoints">

    <table class="table-endpoint">
        <tr>
            <th>Request type</th>
            <th>File</th>
            <th>Parameters</th>
        </tr>
        <tr>
            <td>POST</td>
            <td>cas-start.php</td>
            <td>file = FILE<br>key = API_KEY</td>
        </tr>
    </table>

    <!--    POST /cas-start.php?file=file&key=API_KEY-->
    <p>This endpoint handles POST requests for starting Octave.<br>
        Parameter 'FILE' contains the name of the file to be run in Octave
        (establishes socket connection between Octave and the PHP server).
        <br>
        Parameter 'API_KEY' is used to secure this endpoint.
        Endpoint is unusable without this parameter.
        Macro is defined in 'config-api.php'.
    </p>

    <br><br><br>

    <table class="table-endpoint">
        <tr>
            <th>Request type</th>
            <th>File</th>
            <th>Parameters</th>
        </tr>
        <tr>
            <td>POST</td>
            <td>cas-com.php</td>
            <td>command = CMD<br>key = API_KEY</td>
        </tr>
    </table>

    <p>
        This endpoint handles POST requests for sending commands to Octave.
        <br>
        Parameter 'CMD' contains the command(s) to be run in Octave.
        Results are returned as a response.
        <br>
        Parameter 'API_KEY' is used to secure this endpoint.
        Endpoint is unusable without this parameter.
        Macro is defined in 'config-api.php'.
    </p>

</div>

</body>
</html>