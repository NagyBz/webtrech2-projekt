<?php

include_once "../../CasCommunicator.php";
include_once "configMatyi.php";
include_once "../../Database.php";
include_once "../../CommandLog.php";
include_once "../../CommandLogger.php";
include_once "../../Cas.php";

// create objects
$database = new Database(HOST, USERNAME, PASSWORD, DATABASE);

$logger = new CommandLogger($database);

$cas = new Cas(new CasCommunicator(), $logger);

if (isset($_POST['command']) && $_POST['command']) {

    // get the command string
    $command = $_POST["command"];

    // connect
    if (!$cas->connect("127.0.0.1", 20209)) {

        echo $logger->logIntoDatabase(new CommandLog($command, "false", socket_strerror(socket_last_error())));

        echo "ERROR: " . socket_strerror(socket_last_error());

    } else {

        switch ($command) {

            case "preload":

                echo $cas->preloadParametersForGraph("suspention_launch_paramerter.txt");
                break;

            case "exit":

                $cas->stop();
                $cas->disconnect();
                echo "Exited";
                break;

            default:

                echo $cas->exec($command);
        }
    }
}
?>