
let dataPointsBodyMass = [];
let dataPointsSuspentionMass = [];
let xAxis = [];
let chart = null;


// transale to xy ,back to xy and
function moveSection(idStr, xOffset, yOffset) {
    var domElemnt = document.getElementById(idStr);
    if (domElemnt) {

        var transformAttr = ' translate(' + xOffset + ',' + yOffset + ')';
        domElemnt.setAttribute('transform', transformAttr);
    }
}


let slider = document.getElementById("myRange");
let output = document.getElementById("demo");
output.innerHTML = slider.value;
let r = 0;
slider.oninput = function () {
    output.innerHTML = this.value;
    console.log(slider.value);
}

let running = false;


function clearGraf() {
    dataPointsBodyMass = [];
    dataPointsSuspentionMass = [];
    xAxis = [];
    myChart.reset();
}



function visual() {
    graf1=document.getElementById("graf").checked;
    animation1=document.getElementById("animation").checked;
    if(graf1===false){
        document.getElementById("graf_container").style.visibility="hidden";
    }
    if(graf1===true){
        document.getElementById("graf_container").style.visibility="visible";
    }
    if(animation1===false){
        document.getElementById("car_svg").style.visibility="hidden";
    }
    if(animation1===true){
        document.getElementById("car_svg").style.visibility="visible";
    }
}





function drawGraph() {
    console.log(slider.value);
    console.log("stream_suspention_data.php?key=xradvanyim123&r=" + slider.value);
    if (!running) {

        running = true;

        let source = new EventSource("stream_suspention_data.php?key=xradvanyim123&r=" + slider.value);
        source.addEventListener("message", function (e) {
            //console.log(e.data);
            if ('END' === e.data) {
                source.close();
                running = false;

                return;
            }
            // console.log(e.data);
            createDataPoints(e.data)
        });
    }
}

$(document).ready(function () {



    ctx = document.getElementById("chart").getContext('2d');
    myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: xAxis,
            datasets:
                [{
                    label: "dataPointsBodyMass",
                    data: dataPointsBodyMass,
                    backgroundColor: 'transparent',
                    borderColor: 'rgba(255,99,132)',
                    // borderWidth: 3,
                }, {
                    label: "dataPointsSuspentionMass",
                    data: dataPointsSuspentionMass,
                    backgroundColor: 'transparent',
                    borderColor: 'rgb(68,255,43)',
                    // borderWidth: 3
                }]
        },

        options: {
            scales: {scales: {yAxes: [{stacked: true}], xAxes: [{stacked: true}]}},
            tooltips: {mode: 'index', intersect: false}, responsive: true,
            legend: {
                cursor: "pointer",
                itemclick: function (e) {
                    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    } else {
                        e.dataSeries.visible = true;
                    }

                    e.chart.render();
                }
            }
        }
    });
    myChart.render();


});

function createDataPoints(reply) {
    // visual();

    let data = JSON.parse(reply);


    dataPointsBodyMass.push({ //X1) označuje polohu vrchného telesa (oranžový graf) , t.j.
        x: parseFloat(data["x"]),
        y: parseFloat(data["y1"])
    });
    dataPointsSuspentionMass.push({//x3označuje polohu spodného telesa (modrý graf).
        x: parseFloat(data["x"]),
        y: parseFloat(data["y2"])
    });
    xAxis.push(data["x"]);

    moveSection("svg_1", 0, parseFloat(data["y1"]));
    moveSection("svg_2", 0, parseFloat(data["y2"]));
    console.log(dataPointsBodyMass.length);
    console.log(xAxis.length);
    myChart.update();

}


