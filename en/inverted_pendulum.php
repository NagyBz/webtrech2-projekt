<?php

require_once 'pendulum/config-anim-patrik.php';


include "../log_request.php";
log_page("task1");


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inverted Pendulum</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="index.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="../language.js"  type="application/javascript"></script>
    <link rel="stylesheet" type="text/css" href="pendulum/pendulum.css"/>
    <script src="pendulum/pendulum.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individual Tasks
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="inverted_pendulum.php">Inverted Pendulum</a>
                    <a class="dropdown-item" href="suspention/suspension.php">Suspension</a>
                    <a class="dropdown-item" href="ball_and_beam.php">Ball & Beam</a>
                    <a class="dropdown-item" href="aircraft_pitch.php">Aircraft Pitch</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Information
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="statistics.php">Statistics</a>
                    <a class="dropdown-item" href="documentation.php">API Documentation</a>
                    <a class="dropdown-item" href="tasks.php">Task Management</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Language
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">English</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovak</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<br>
<input type="range" id="r-range" min="-0.3" max="0.3" step="0.01" value="0.2" oninput="showR(this.value)">
r: <span id="r-range-p">0.2</span>
<br>

<input type="checkbox" id="ckbx-show-graph" checked="checked" onchange="showGraph()">
<label for="ckbx-show-graph">Show Graph</label>
<br>

<input type="checkbox" id="ckbx-show-anim" checked="checked" onchange="showAnim()">
<label for="ckbx-show-anim">Show Animation</label>
<br><br>

<button id="btn-draw-graph" type="button" onclick="draw(this, <?php echo ANIMATION_FREQUENCY; ?>)">
    <img src="pendulum/start-icon.png" alt="start-icon" width="30" height="30">
</button>

<section id="pendulum-container">

    <div id="div-chart">

        <div id="chart"></div>

    </div>

    <div id="div-anim">

        <canvas id="canvas-pendulum" width="800" height="400"></canvas>

    </div>

</section>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

</body>
</html>