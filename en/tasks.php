<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="index.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="../language.js"  type="application/javascript"></script>
    <title>Tasks</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individual Tasks
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="inverted_pendulum.php">Inverted Pendulum</a>
                    <a class="dropdown-item" href="suspention/suspension.php">Suspension</a>
                    <a class="dropdown-item" href="ball_and_beam.php">Ball & Beam</a>
                    <a class="dropdown-item" href="aircraft_pitch.php">Aircraft Pitch</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Information
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="statistics.php">Statistics</a>
                    <a class="dropdown-item" href="documentation.php">API Documentation</a>
                    <a class="dropdown-item" href="tasks.php">Task Management</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Language
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">English</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovak</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<div id="task-table">
    <h2>Tasks</h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Balázs</th>
        <th scope="col">Mátyás</th>
        <th scope="col">Patrik</th>
        <th scope="col">Zoltán</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Languages</th>
        <td>&#10004;</td>
        <td>&#10004;</td>
        <td>&#10004;</td>
        <td>&#10004;</td>
    </tr>
    <tr>
        <th scope="row">CAS API</th>
        <td></td>
        <td>&#10004;</td>
        <td>&#10004;</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">API key</th>
        <td></td>
        <td></td>
        <td></td>
        <td>&#10004;</td>
    </tr>    <tr>
        <th scope="row">Logging</th>
        <td></td>
        <td></td>
        <td>&#10004;</td>
        <td></td>
    </tr>
        <tr>
        <th scope="row">Task Mangement</th>
        <td>&#10004;</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>  <tr>
        <th scope="row">Design</th>
        <td></td>
        <td></td>
        <td></td>
        <td>&#10004;</td>
    </tr>
    <tr>
        <th scope="row">Statistics and Email</th>
        <td>&#10004;</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">API Documentation</th>
        <td></td>
        <td></td>
        <td>&#10004;</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">PDF, CSV export</th>
        <td></td>
        <td>&#10004;</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Inverted pendulum</th>
        <td></td>
        <td></td>
        <td>&#10004;</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Ball and beam</th>
        <td>&#10004;</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Suspension</th>
        <td></td>
        <td>&#10004;</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Aircraft pitch</th>
        <td></td>
        <td></td>
        <td></td>
        <td>&#10004;</td>
    </tr>
    </tbody>
</table>
</div>
</body>
</html>