<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="index.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="index.js" type="application/javascript"></script>
    <script src="../language.js"  type="application/javascript"></script>
</head>
<body id="index-body">
<nav class="navbar navbar-expand-lg navbar-dark">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individual Tasks
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="inverted_pendulum.php">Inverted Pendulum</a>
                    <a class="dropdown-item" href="suspention/suspension.php">Suspension</a>
                    <a class="dropdown-item" href="ball_and_beam.php">Ball & Beam</a>
                    <a class="dropdown-item" href="aircraft_pitch.php">Aircraft Pitch</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Information
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="statistics.php">Statistics</a>
                    <a class="dropdown-item" href="documentation.php">API Documentation</a>
                    <a class="dropdown-item" href="tasks.php">Task Management</a>
                </div>
            </li>
        </ul>
        <div class="btn-group">
            <button type="button" id="export-button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Export to...
            </button>
            <div class="dropdown-menu">
                <a href="../Matyi/logs_export_to_pdf.php" class="dropdown-item export" type="button">PDF</a>
                <a href="../Matyi/logs_export_to_csv.php" class="dropdown-item export" type="button">CSV</a>
            </div>
        </div>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Language
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">English</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovak</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<div class="container" >
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <h4 id="welcomeText"></h4>
                <ul id="ul-help">
                    <li id="li-home"></li>
                    <li id="li-help"></li>
                    <li id="li-clear"></li>
                    <li id="li-start"></li>
                    <li id="li-exit"></li>
                </ul>
                <div id="div-log-commands"></div>
                <div class="form-group" id="div-octave-input">
                    <label for="input-command">cli:~$</label>
                    <input type="text" id="input-command" class="mx-sm-1 text-secondary" autocomplete="off" name="command">
                    <p id="reply"></p>
                </div>
        </div>
    </div>
</div>
</body>
</html>