<?php

include_once "../../Cas.php";
include_once "config-api.php";

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["key"]) && $_POST["key"] == API_KEY) {

    try {

        $file = $_POST["file"];

        Cas::start($file);

    } catch (Exception $e) {

        echo "Error occurred while starting CAS: " . $e->getMessage();
    }
}

?>