$(document).ready(function () {

    diagram($("#doc-title"), $("#doc-title").html());

    createDiagrams()
});

function openNav() {

    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    document.getElementById("div-diagram").style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {

    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "white";
    document.getElementById("div-diagram").style.backgroundColor = "white";
}

function changeDoc(title) {

    switch (title) {

        case "Class diagram":
        case "Diagram tried":
            diagram($("#doc-title"), title);
            break;
        case "Classes and methods":
        case "Triedy a metódy":
            classes($("#doc-title"), title);
            break;
        case "Endpoints":
        case "Koncové body":
            endpoints($("#doc-title"), title);
            break;
    }

    closeNav();
}

function endpoints(docTitle, title) {

    hideDiagram();
    hideTable();

    docTitle.html(title);

    showEndpoint();
}

function classes(docTitle, title) {

    hideDiagram();
    hideEndpoint();

    docTitle.html(title);

    showTable();
}

function diagram(docTitle, title) {

    hideTable();
    hideEndpoint();

    docTitle.html(title);

    showDiagram();
}

function showEndpoint() {

    $("#div-endpoints").show();
}

function hideEndpoint() {

    $("#div-endpoints").hide();
}

function showTable() {

    $("#div-table").show();
}

function hideTable() {

    $("#div-table").hide();
}

function showDiagram() {

    $("#div-diagram").show();
}

function hideDiagram() {

    $("#div-diagram").hide();
}

function createDiagrams() {

    let $ = go.GraphObject.make;

    let diagram = $(go.Diagram, "div-diagram",
            { // enable Ctrl-Z to undo and Ctrl-Y to redo
                "undoManager.isEnabled": true,
                allowCopy : false,
                allowDelete : false,
                allowInsert: false
            });

    let nodeDatabase = $(go.Node, "Auto", { position: new go.Point(1200, -100) },
        $(go.Panel, "Vertical",
            { background: "#2b2b2b", defaultStretch: go.GraphObject.Horizontal, padding: 5 },
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/class-icon.png", width: 25, height: 25, margin: 5}),
                $(go.TextBlock, "Database", { margin: 5, stroke: "white", font: "Bold 20px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/field-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "connection", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__construct(host, user, password, schema)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__destruct()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "runQuery(sql)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" }))));

    diagram.add(nodeDatabase);

    let nodeCommandLogger = $(go.Node, "Auto", { position: new go.Point(800, -50) },
        $(go.Panel, "Vertical",
            { background: "#2b2b2b", defaultStretch: go.GraphObject.Horizontal, padding: 5 },
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/class-icon.png", width: 25, height: 25, margin: 5}),
                $(go.TextBlock, "CommandLogger", { margin: 5, stroke: "white", font: "Bold 20px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/field-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "database", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__construct(database)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__destruct()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "logIntoDatabase(commandLog)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" }))));

    diagram.add(nodeCommandLogger);

    let nodeCasCommunicator = $(go.Node, "Auto", { position: new go.Point(-50, 100) },
        $(go.Panel, "Vertical",
            { background: "#2b2b2b", defaultStretch: go.GraphObject.Horizontal, padding: 5 },
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/class-icon.png", width: 25, height: 25, margin: 5}),
                $(go.TextBlock, "CasCommunicator", { margin: 5, stroke: "white", font: "Bold 20px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/field-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "socket", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__construct(domain, type, protocol)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__destruct()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "connect(host, port)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "disconnect()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "receive(length)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "send(message)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" }))));

    diagram.add(nodeCasCommunicator);

    let nodeCas = $(go.Node, "Auto", { position: new go.Point(400, -50) },
        $(go.Panel, "Vertical",
            { background: "#2b2b2b", defaultStretch: go.GraphObject.Horizontal, padding: 5 },
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/class-icon.png", width: 25, height: 25, margin: 5}),
                $(go.TextBlock, "Cas", { margin: 5, stroke: "white", font: "Bold 20px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/field-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "logger", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/field-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "communicator", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__construct(communicator, logger)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__destruct()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "contains(haystack, needle)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "start(file, port)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "stop()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "connect(host, port)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "disconnect()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "exec(command, logCommand)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "log(command, reply)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "preloadParametersForGraph(file)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "setCommunicator(communicator)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "getCommunicator()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" }))));

    diagram.add(nodeCas);

    let nodeCommandLog = $(go.Node, "Auto", { position: new go.Point(900, 200) },
        $(go.Panel, "Vertical",
            { background: "#2b2b2b", defaultStretch: go.GraphObject.Horizontal, padding: 5 },
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/class-icon.png", width: 25, height: 25, margin: 5}),
                $(go.TextBlock, "CommandLog", { margin: 5, stroke: "white", font: "Bold 20px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/field-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "command", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/field-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "commandCorrect", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/field-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-closed.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "errorMessage", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__construct(command, commandCorrect, errorMessage)", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "__destruct()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "getCommand()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "getCommandCorrect()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" })),
            $(go.Panel, "Horizontal",
                { defaultStretch: go.GraphObject.Horizontal },
                $(go.Picture, { source: "../api-doc-styles/method-icon.png", width: 15, height: 15, margin: 5}),
                $(go.Picture, { source: "../api-doc-styles/lock-icon-opened.png", width: 15, height: 15, margin: 5}),
                $(go.TextBlock, "getErrorMessage()", { margin: 5, stroke: "white", font: "Bold 15px sans-serif" }))));

    diagram.add(nodeCommandLog);

    diagram.add(
        $(go.Link,
            { fromNode: nodeDatabase, toNode: nodeCommandLogger, routing: go.Link.AvoidsNodes, corner: 10, toShortLength: 10, curve: go.Link.JumpGap },
            $(go.Shape, { strokeWidth: 4 }),
            $(go.Shape, { toArrow: "Standard", scale: 1.5 })
        ));

    diagram.add(
        $(go.Link,
            { fromNode: nodeCommandLogger, toNode: nodeCas, routing: go.Link.AvoidsNodes, corner: 10, toShortLength: 10, curve: go.Link.JumpGap },
            $(go.Shape, { strokeWidth: 4 }),
            $(go.Shape, { toArrow: "Standard", scale: 1.5 })
        ));

    diagram.add(
        $(go.Link,
            { fromNode: nodeCasCommunicator, toNode: nodeCas, routing: go.Link.AvoidsNodes, corner: 10, toShortLength: 10, curve: go.Link.JumpGap },
            $(go.Shape, { strokeWidth: 4 }),
            $(go.Shape, { toArrow: "Standard", scale: 1.5 })
        ));

    diagram.add(
        $(go.Link,
            { fromNode: nodeCommandLog, toNode: nodeCommandLogger, routing: go.Link.AvoidsNodes, corner: 10, toShortLength: 10, curve: go.Link.JumpGap },
            $(go.Shape, { strokeWidth: 4 }),
            $(go.Shape, { toArrow: "Standard", scale: 1.5 })
        ));
}

function sortTableString(tableId, n)
{
    let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;

    table = document.getElementById(tableId);

    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        //console.log(rows.length);
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount ++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}


function sortTableNumeric(tableId, n)
{
    let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;

    table = document.getElementById(tableId);

    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (dir == "asc") {
                if (Number(x.innerHTML) > Number(y.innerHTML)) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch= true;
                    break;
                }
            } else if (dir == "desc") {
                if (Number(x.innerHTML) < Number(y.innerHTML)) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }

        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount ++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

function checkboxController(tableId, checkbox)
{
    if (checkbox.checked) {
        toggleTableColumn(tableId, checkbox.value, true);
    } else {
        toggleTableColumn(tableId, checkbox.value, false);
    }
}

function toggleTableColumn(tableid, value, bShow)
{
    let table = document.getElementById(tableid);

    let tr = table.getElementsByTagName("tr");

    for (let i = 0; i < tr.length; i++)
    {
        let td = tr[i].getElementsByTagName("td")[0];

        if (td)
        {
            let txtValue = td.textContent || td.innerText;

            if (txtValue === value) {

                if (bShow) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none"
                }
            }
        }
    }
}