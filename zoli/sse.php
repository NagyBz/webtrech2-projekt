<?php

include_once "../CasCommunicator.php";
include_once "zoli-config.php";
include_once "../Database.php";
include_once "../CommandLog.php";
include_once "../CommandLogger.php";
include_once "../Cas.php";



if ($_SERVER["REQUEST_METHOD"] == "GET"&& isset($_GET["key"]) && $_GET["key"] == "patrik123") {
    header('Content-Type: text/event-stream');
    header('Cache-Control: no-cache');

    $data = null;

    // CAS
    // start cas
    Cas::start("start.m");

    // create objects
    $database = new Database(HOST, USERNAME, PASSWORD, DATABASE);
    $logger = new CommandLogger($database);
    $cas = new Cas(new CasCommunicator(), $logger);



    if (!$cas->connect("127.0.0.1", 20206)) {

        echo $logger->logIntoDatabase(new CommandLog("connect to CAS", "false", socket_strerror(socket_last_error())));

        echo "Socket connection error: " . socket_strerror(socket_last_error());

        $data = null;

    } else {

        $params = $cas->preloadParametersForGraph("aircraft_pitch_parameter.txt");

        $cas->stop();
        $cas->disconnect();

        $data = json_decode($params);
    }

    // SSE
    if (!is_null($data)) {

        foreach ($data as $datum) {

            echo "data: {\n";
            echo "data: \"x\":\"$datum[0]\",\n";
            echo "data: \"y1\":\"$datum[2]\",\n";
            echo "data: \"y2\":\"$datum[1]\"\n";
            echo "data: }\n\n";
            @ob_flush();
            flush();
            if (connection_aborted()) {
                break;
            }

            usleep(40000);
        }

        // stop SSE
        echo "data: END-OF-STREAM\n\n";
        @ob_flush();
        flush();
        sleep(1);
    }

}

?>