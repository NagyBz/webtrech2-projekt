<?php

include_once "CommandLog.php";

class Cas
{
    private $communicator;
    private $logger;

    public function __construct(CasCommunicator $communicator, CommandLogger $logger) {

        $this->communicator = $communicator;
        $this->logger = $logger;
    }

    public static function start($file, $port = 20205) {

        shell_exec("octave $file $port");
    }

    public function preloadParametersForGraph($file) {

        $results = "";

        $lines = file($file);

        foreach ($lines as $line) {

            $results .= $line;
        }

        //$this->send("M = .5; m = 0.2;b = 0.1;I = 0.006;g = 9.8;l = 0.3;p = I*(M+m)+M*m*l^2;A = [0 1 0 0; 0 -(I+m*l^2)*b/p (m^2*g*l^2)/p 0; 0 0 0 1; 0 -(m*l*b)/p m*g*l*(M+m)/p 0];B = [ 0; (I+m*l^2)/p; 0; m*l/p];C = [1 0 0 0; 0 0 1 0];D = [0; 0];K = lqr(A,B,C'*C,1);Ac = [(A-B*K)];N = -inv(C(1,:)*inv(A-B*K)*B);sys = ss(Ac,B*N,C,D);t = 0:0.05:10;r =0.2;initPozicia=0;initUhol=0;[y,t,x]=lsim(sys,r*ones(size(t)),t,[initPozicia;0;initUhol;0]);");

        return $this->exec($results);
    }

    public function connect($host = "127.0.0.1", $port = 20205) {

        return $this->communicator->connect($host, $port);
    }

    public function exec($command, $logCommand = true) {

        // send message
        $this->communicator->send($command);

        // receive reply
        $reply = trim($this->communicator->receive());

        if ($logCommand) {

            $this->log($command, $reply);
        }

        return $reply;
    }

    public function stop() {

        $this->log("exit", null);

        $this->communicator->send("exit");
    }

    public function disconnect() {

        $this->communicator->disconnect();
    }

    private function log($command, $reply) {

        // log into database
        $isCmdOk = !$this->contains($reply, "ERROR");

        $log = new CommandLog($command,
            $isCmdOk ? "true" : "false",
            $isCmdOk ? "null" : substr($reply, 7));

        return $this->logger->logIntoDatabase($log);
    }

    private function contains($haystack, $needle) {

        return strpos($haystack, $needle) !== false;
    }

    public function getCommunicator() {

        return $this->communicator;
    }

    public function setCommunicator($communicator) {

        $this->communicator = $communicator;
    }
}
