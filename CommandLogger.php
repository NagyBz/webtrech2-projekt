<?php


class CommandLogger
{
    private $database;

    public function __construct(Database $database) {

        $this->database = $database;
    }

    public function logIntoDatabase(CommandLog $commandLog) {

        $query = "INSERT INTO logs (commands, command_correct, error_message) VALUES (\"" . $commandLog->getCommand() . "\", " . $commandLog->getCommandCorrect() . " , \"" . $commandLog->getErrorMessage() . "\")";

        return $this->database->runQuery($query);
    }
}
