<!DOCTYPE html>
<html>
<body>

<canvas id="myCanvas" width="500" height="500" style="border:1px solid #d3d3d3;">
    Your browser does not support the HTML5 canvas tag.</canvas>

<script>
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    var y=10;
    var x=250;

    function drawBall(){
        console.log(x);
        console.log(y);
        ctx.clearRect(0,0,500,500);
        y++;
        ctx.fillStyle="green";
        ctx.beginPath();
        ctx.arc(x, y, 20, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.fill();
        setInterval(drawBall,10);
    }

drawBall();

</script>

</body>
</html>
