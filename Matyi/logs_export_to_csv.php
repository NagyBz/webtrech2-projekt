<?php
include('../zoli/zoli-config.php');
include('../Database.php');

$database = new Database(HOST,USERNAME,PASSWORD,DATABASE);
$result = $database->runQuery("SELECT * FROM logs");

header('Content-type: text/csv');
header('Content-Disposition: attachment; filename="filename.csv"');

$output = fopen('php://output', 'w');
foreach ($result as $row) {

    fputcsv($output, $row);
}
fclose($output);

?>