# load socket package
pkg load sockets;

# create socket
s = socket(AF_INET, SOCK_STREAM, 0);

# bind socket to port
bind(s, 20205);

# listen on the specified port
listen(s, 1);

while (true)

    # accept connection to socket from client
    conn = accept(s);

    # receive message from client
    msg = recv(conn, 100);

    # convert message to string (human readable format)
    msg_str = char(msg);

    # exit on exit
    # if (strcmp(msg_str, "exit") == 1)
    #     msg_str = "1";
    # endif

    # try to evaluate
    try

        # evaluate expression
        result_num = eval(msg_str);

        # convert number to string
        reply_str = num2str(result_num);

        # send reply
        send(conn, reply_str);

    catch

        # error caught
        error_message = lasterror.message;

        # send back error message
        send(conn, error_message);

    end_try_catch

endwhile