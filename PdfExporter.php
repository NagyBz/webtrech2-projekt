<?php
include_once "Matyi/fpdf182/fpdf.php";

class PdfExporter
{
    private $pdf;

    public function __construct() {

        $this->pdf = new FPDF();
        $this->pdf->AddPage();
        $this->pdf->SetFont('Arial','B',8);
    }

    function exportResultSet($resultSet) {

        foreach ($resultSet as $row) {

            $this->pdf->Ln();

            foreach ($row as $column) {

                $this->pdf->MultiCell(185, 6, $column, 1);
            }
        }

        $this->pdf->Output();
    }

    function exportText($text) {

        $this->pdf->Write(5,$text);
        $this->pdf->Output();
    }
}

