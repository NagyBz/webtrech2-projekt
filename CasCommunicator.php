<?php


class CasCommunicator
{
    private $socket;

    public function __construct($domain = AF_INET, $type = SOCK_STREAM, $protocol = 0) {

        $this->socket = socket_create($domain, $type, $protocol);
    }

    public function __destruct()
    {
        $this->disconnect();
    }

    public function connect($host = "127.0.0.1", $port = 20205) {

        return socket_connect($this->socket, $host, $port);
    }

    public function send($message) {

        return socket_write($this->socket, $message, strlen($message));
    }

    public function receive($length = 20000) {

        return socket_read($this->socket, $length);
    }

    public function disconnect() {

        socket_shutdown($this->socket);
        socket_close($this->socket);
    }
}
