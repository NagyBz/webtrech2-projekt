<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <title>API Dokumentácia</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="index.css" type="text/css">
    <link rel="stylesheet" href="../api-doc-styles/api-doc.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/gojs/release/go-debug.js"></script>
    <script src="../language.js"></script>
    <script src="../api-doc-styles/api-doc.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Domov</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individuálne úlohy
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="inverted_pendulum.php">Inverzné kývadlo</a>
                    <a class="dropdown-item" href="suspention/suspension.php">Tlmič automobilu</a>
                    <a class="dropdown-item" href="ball_and_beam.php">Gulička na tyči</a>
                    <a class="dropdown-item" href="aircraft_pitch.php">Náklon lietadla</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Informácie
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="statistics.php">Štatistika</a>
                    <a class="dropdown-item" href="documentation.php">API Dokumentácia</a>
                    <a class="dropdown-item" href="tasks.php">Rozdelenie úloh</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Jazyk
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">Anglický</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovenský</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div id="mySidenav" class="sidenav">

    <div class="closebtn" onclick="closeNav()">&times;</div>
    <div onclick="changeDoc('Diagram tried')">Diagram&nbsp;tried</div>
    <div onclick="changeDoc('Triedy a metódy')">Triedy&nbsp;a&nbsp;metódy</div>
    <div onclick="changeDoc('Koncové body')">Koncové&nbsp;body</div>

</div>

<div id="main">

    <span id="span-open-nav" onclick="openNav()">&#9776;</span>
    <h2 id="doc-title">Diagram tried</h2>

</div>

<div id="div-diagram"></div>

<div id="div-table" >

    <h4>Popis metód</h4>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-cas" name="Cas" value="Cas" onchange="checkboxController('table-method', this)">
    <label for="checkbox-cas">Cas</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-cas-comm" name="CasCommunicator" value="CasCommunicator" onchange="checkboxController('table-method', this)">
    <label for="checkbox-cas-comm">CasCommunicator</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-command-logger" name="CommandLogger" value="CommandLogger" onchange="checkboxController('table-method', this)">
    <label for="checkbox-command-logger">CommandLogger</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-database" name="Database" value="Database" onchange="checkboxController('table-method', this)">
    <label for="checkbox-database">Database</label><br>

    <table id="table-method">
        <tr>
            <th onclick="sortTableString('table-method', 0)">Trieda</th>
            <th onclick="sortTableString('table-method', 1)">Metóda</th>
            <th onclick="sortTableString('table-method', 2)">Argument(y): typ</th>
            <th onclick="sortTableString('table-method', 3)">Návratová hodnota</th>
            <th onclick="sortTableString('table-method', 4)">Modifikátor prístupu</th>
            <th onclick="sortTableString('table-method', 5)">Popis</th>
        </tr>
        <tr>
            <td>CommandLogger</td>
            <td>logIntoDatabase</td>
            <td>commandLog: CommandLog</td>
            <td>Návratová hodnota zavolanej metódy<br>'runQuery' (ResultSet alebo string)</td>
            <td>public</td>
            <td>Metóda slúži na uloženie objektu 'CommandLog' do databázy</td>
        </tr>
        <tr>
            <td>Database</td>
            <td>runQuery</td>
            <td>sql: string</td>
            <td>Výsledok dopytu (ResultSet) v prípade<br>úspešného vykonania dopytu;<br>
                chybová hláška vo forme znakového<br>reťazca v prípade zlyhania
            </td>
            <td>public</td>
            <td>Metóda slúži na vykonanie SQL dopytov</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>connect</td>
            <td>host: string<br>port: int</td>
            <td>TRUE v prípade úspechuô<br>FALSE v prípade zlyhania</td>
            <td>public</td>
            <td>Inicializuje pripojenie socketu s danou adresou a portom</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>disconnect</td>
            <td></td>
            <td>TRUE v prípade úspechuô<br>FALSE v prípade zlyhania</td>
            <td>public</td>
            <td>Uzatvára prepojenie</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>send</td>
            <td>message: string</td>
            <td>Počet bytov zapísaných na socket;<br>FALSE v prípade zlyhania</td>
            <td>public</td>
            <td>Odosiela príkaz (dáta) cez socket</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>receive</td>
            <td>length: int</td>
            <td>Počet bytov prijatých zo socketu</td>
            <td>public</td>
            <td>Získava odpoveď (dáta) cez socket</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>contains</td>
            <td>haystack: string<br>needle: string</td>
            <td>TRUE, ak 'needle' je podreťazcom 'haystack'<br>FALSE v opačnom prípade</td>
            <td>private</td>
            <td>Metóda zistí, či 'needle' je podreťazcom 'haystack'</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>start</td>
            <td>file: string<br>port: int</td>
            <td>Chybová hláška vo forme znakového<br>reťazca v prípade zlyhania</td>
            <td>public static</td>
            <td>Spúšťa Octave a spúšťa kód zo súboru 'file'.<br>Parameter 'port' sa zadáva ako argument programu</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>stop</td>
            <td></td>
            <td></td>
            <td>public</td>
            <td>Pošle príkaz do Octavu na ukončenie</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>connect</td>
            <td>host: string<br>port: int</td>
            <td>Návratová hodnota metódy<br>'CasCommunicator::connect'</td>
            <td>public</td>
            <td>Vytvára prepojenie s Octavom.<br>Deleguje metódu 'connect' triedy 'CasCommunicator'</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>disconnect</td>
            <td></td>
            <td>Návratová hodnota metódy<br>'CasCommunicator::disconnect'</td>
            <td>public</td>
            <td>Uzatvára prepojenie s Octavom.<br>Deleguje metódu 'disconnect' triedy 'CasCommunicator'</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>exec</td>
            <td>command: string<br>logCommand: boolean</td>
            <td>Výsledok vykonania príkazu</td>
            <td>public</td>
            <td>Odosiela príkaz (command) do Octave, ktorý ho následne spúšťa.<br>
                V prípade chyby vracia informáciu o chybe.<br>
                Ak znak 'logCommand' je nastavený na TRUE, loguje sa príkaz</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>log</td>
            <td>command: string<br>reply: string</td>
            <td>Návratová hodnota metódy<br>'Logger::logIntoDatabase'</td>
            <td>private</td>
            <td>Loguje informácie o príkaze<br>(príkaz, úspech vykonania, prípadnú chybovú hlášku)</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>preloadParametersForGraph</td>
            <td>file: string</td>
            <td>Návratová hodnota metódy 'Cas::exec'</td>
            <td>public</td>
            <td>Pospúšťa predpripravené príkazy s dátami ku grafom</td>
        </tr>
    </table>

    <br><br>

    <h4>Popis atribútov</h4>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-cas-atr" name="Cas" value="Cas" onchange="checkboxController('table-field', this)">
    <label for="checkbox-cas-atr">Cas</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-cas-comm-atr" name="CasCommunicator" value="CasCommunicator" onchange="checkboxController('table-field', this)">
    <label for="checkbox-cas-comm-atr">CasCommunicator</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-command-logger-atr" name="CommandLogger" value="CommandLogger" onchange="checkboxController('table-field', this)">
    <label for="checkbox-command-logger-atr">CommandLogger</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-command-log-atr" name="CommandLog" value="CommandLog" onchange="checkboxController('table-field', this)">
    <label for="checkbox-command-log-atr">CommandLog</label>

    <input checked="checked" class="table-checkboxes" type="checkbox" id="checkbox-database-atr" name="Database" value="Database" onchange="checkboxController('table-field', this)">
    <label for="checkbox-database-atr">Database</label><br>

    <table id="table-field">
        <tr>
            <th onclick="sortTableString('table-field', 0)">Trieda</th>
            <th onclick="sortTableString('table-field', 1)">Atribút</th>
            <th onclick="sortTableString('table-field', 2)">Dátový typ</th>
            <th onclick="sortTableString('table-field', 3)">Modifikátor prístupu</th>
            <th onclick="sortTableString('table-field', 4)">Popis</th>
        </tr>
        <tr>
            <td>CommandLog</td>
            <td>command</td>
            <td>string</td>
            <td>private</td>
            <td>Príkaz, ktorý zadal užívateľ v CLI</td>
        </tr>
        <tr>
            <td>CommandLog</td>
            <td>commandCorrect</td>
            <td>boolean</td>
            <td>private</td>
            <td>Označuje, či príkaz bol vykonaný správne, alebo nastala nejaká chyba</td>
        </tr>
        <tr>
            <td>CommandLog</td>
            <td>errorMessage</td>
            <td>string</td>
            <td>private</td>
            <td>Chybová hláška, ktorú vrátil CAS</td>
        </tr>
        <tr>
            <td>CommandLogger</td>
            <td>database</td>
            <td>Database</td>
            <td>private</td>
            <td>Inštancia triedy 'Database', pomocou ktorej sú údaje o príkaze zapísané do databázy</td>
        </tr>
        <tr>
            <td>Database</td>
            <td>connection</td>
            <td>mysqli_connection</td>
            <td>private</td>
            <td>Prostredníctvom tohto objektu sú posielané dopyty na databázový server</td>
        </tr>
        <tr>
            <td>CasCommunicator</td>
            <td>socket</td>
            <td>PHP socket</td>
            <td>private</td>
            <td>Inštancia PHP socketu, ktorá umožňuje vytvárať prepojenie s Octavom, ktorý beží paralelne</td>
        </tr>

        <tr>
            <td>Cas</td>
            <td>logger</td>
            <td>CommandLogger</td>
            <td>private</td>
            <td>Inštancia triedy 'CommandLogger', slúži na logovanie príkazov</td>
        </tr>
        <tr>
            <td>Cas</td>
            <td>communicator</td>
            <td>CasCommunicator</td>
            <td>private</td>
            <td>Inštancia triedy 'CasCommunicator', ktorá deleguje (niektoré) metódy tejto triedy</td>
        </tr>
    </table>
</div>

<div id="div-endpoints">

    <table class="table-endpoint">
        <tr>
            <th>Typ požiadavky</th>
            <th>Súbor</th>
            <th>Parametre</th>
        </tr>
        <tr>
            <td>POST</td>
            <td>cas-start.php</td>
            <td>file = FILE<br>key = API_KEY</td>
        </tr>
    </table>

<!--    POST /cas-start.php?file=file&key=API_KEY-->
    <p>Tento koncový bod spracováva POST požiadavku a spustí Octave.
        <br>
        Parameter 'FILE' obsahuje súbor, ktorý sa má spustiť v prostredí Octave
        (nadväzuje prepojenie medzi Octavom a PHP serverom cez socket).
        <br>
        Parameter 'API_KEY' slúži na zabezpečenie koncového bodu.
        Bez zadania tohto parametra sa nedá zavolať koncový bod.
        Makro je definované v súbore 'config-api.php'.
        <br>
        Koncový bod nevracia odpoveď.</p>

    <br><br><br>

    <table class="table-endpoint">
        <tr>
            <th>Typ požiadavky</th>
            <th>Súbor</th>
            <th>Parametre</th>
        </tr>
        <tr>
            <td>POST</td>
            <td>cas-com.php</td>
            <td>command = CMD<br>key = API_KEY</td>
        </tr>
    </table>

    <p>Tento koncový bod spracováva POST požiadavku a posiela príkazy Octavu cez socket.
        <br>
        Parameter 'CMD' obsahuje príkaz vo forme znakového reťazca, ktorý sa má vykonať v prostredí Octave.
        V odpovedi sa vracajú výsledky príkazu z Octave.
        <br>
        Parameter 'API_KEY' slúži na zabezpečenie koncového bodu.
        Bez zadania tohto parametra sa nedá zavolať koncový bod.
        Makro je definované v súbore config-api.php.</p>

</div>

</body>
</html>