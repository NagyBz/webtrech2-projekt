var octaveIsRunning = false;

$( document ).ready(function() {
    writeWelcomeText();

    $('#input-command').keyup(function(e){
        if(e.which == 13) {

            let command = $('#input-command').val();

            switch(command) {
                case "help":
                    command = help();
                    break;
                case "clear":
                    command = clear();
                    break;
                case "home":
                    command = home();
                    break;
                case "start":
                    command = start();
                    break;
                case "exit":
                    command = exit();
                    break;
                default:
                    command = "";
            }

            // logCommand();

            if (command !== "")
            {
                clearCommandList($('#input-command').val());
                typeWriter(0, command, 50, "welcomeText");
                showCommandLine(50, command.length);
            }
            else if(octaveIsRunning && $('#input-command').val() !== "start")
            {
                executeOctaveCommand();
            }
            else
            {
                if(!octaveIsRunning)
                {
                    $('#div-log-commands').empty();
                    typeWriter(0, "Neznámy príkaz!", 50, "div-log-commands");
                }

            }
            $('#input-command').val("");


        }
    });

});

function clearCommandList(command)
{
    $('#welcomeText').empty();
    $('#div-log-commands').empty();
    $('#reply').empty();
    $('#input-command').val("");
    $('#div-octave-input').css('display','none');
    if (command !== "help")
    {
        $('#ul-help').css('display', 'none');
    }

}

function logCommand() {
    let val = $('#input-command').val();
    let log = $("<p id='p-logged-command' class='text-secondary'><span id='span-octave'>cli:~$ </span> " + val + "</p>")
    $('#input-command').val("");
    $('#div-log-commands').append(log);
}

function writeWelcomeText()
{
    let speed = 50;
    let text = "Víta Vás Octave CLI - zadajte príkaz a stlačte ENTER. Pre ďalšie informácie zadajte 'help'...";

    typeWriter(0, text, speed, "welcomeText");
    showCommandLine(speed, text.length);
}

function showCommandLine(speed, length)
{
    setTimeout(function () {
        $('#div-octave-input').css('display','block');
    }, speed * length);
}

function typeWriter(i, txt, speed, elementId) {
    if (i < txt.length) {
        document.getElementById(elementId).innerHTML += txt.charAt(i);
        i++;
        setTimeout(function(){
            typeWriter(i++, txt, speed, elementId);
        }, speed);
    }
}

function help() {
    createHelpField();
    return "Potrebujete pomoc?";
}

function clear() {
    return 'Vymazané!';
}

function home() {
    return "Víta Vás Octave CLI - zadajte príkaz a stlačte ENTER. Pre ďalšie informácie zadajte 'help'...";
}

function start() {
    startCas('../zoli/start.m');
    octaveIsRunning = true;
    return "Octave je aktívne";
}

function exit()
{
    octaveIsRunning = false;
    return "Octave je pozastavené";
}

function startCas(file) {

    // sent request for starting CAS
    $.ajax({
        method: "POST",
        url: "pendulum/cas-start.php",
        data: { "file": file },
        error: function (response) {
            alert(response.length);
        }
    });
}

function isValidJson(str) {

    try {

        JSON.parse(str);

    } catch (e) {
        return false;
    }
    return true;
}

function executeOctaveCommand()
{
    let cmd = $("#input-command").val();

    $("#reply").load("../zoli/zoli-cas.php", {

        command: cmd

    }, function(response, status) {

        console.log("Response: " + response);
        console.log("Status: " + status);
        console.log("Valid JSON: " + isValidJson(response));

        if (isValidJson(response)) {
            console.log(response);
        }

    });
}

function createHelpField() {
    $('#ul-help').css('display', 'block');

    $('#li-home').empty();
    $('#li-start').empty();
    $('#li-exit').empty();
    $('#li-help').empty();
    $('#li-clear').empty();

    let help_text = "'help' - Vylistuje všetky príkazy, ktoré sú dostupné v príkazovom riadku.";
    let start_text = "'start' - Spustenie Octavu. Bez tohto príkazu nie je možné komunikovať s Octavom.";
    let exit_text = "'exit' - Ukončí komunikáciu s Octavom.";
    let home_text = "'home' - Návrat na domovskú stránku.";
    let clear_text = "'clear' - Vyčistí lištu spustených príkazov.";

    typeWriter(0, home_text, 30, "li-home");
    typeWriter(0, help_text, 30, "li-help");
    typeWriter(0, clear_text, 30, "li-clear");
    typeWriter(0, start_text, 30, "li-start");
    typeWriter(0, exit_text, 30, "li-exit");
}


