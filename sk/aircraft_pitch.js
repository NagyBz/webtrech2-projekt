aircraftPositionPoints = [{ x: 0, y: 0 }];
bankingPoints = [{ x: 0, y: 0 }];
chart = null;

$(document).ready(function () {
    drawGraph();
    startCas('../zoli/start.m');
    sendCommandToCAS("preload");

    $('#checkbox-graph').click(function() {
        $("#div-graph").toggle(this.checked);
    });

    $('#checkbox-animation').click(function() {
        $("#div-animation").toggle(this.checked);
    });
});

function goodbye() {
    sendCommandToCAS("exit");
}

window.onbeforeunload=goodbye;


function getData(frequency, button) {

    button.disabled = true;

    // request for changing 'r'
    sendCommandToCAS("r = " + $("#input-slider").val());

    // request for showing graph with new 'r'
    let cmd = "[y,t,x]=lsim(sys,r*ones(size(t)),t,x(size(x,1),:));\n" + "object2json([t, y, (r*ones(size(t))*N-x*K')])";
    let response = sendCommandToCAS(cmd);

    if (isValidJson(response)) {

        response = JSON.parse(response);
        drawChartAnim(frequency, response)
            .then(() => { button.disabled = false; });
    }

}

function createDataPoints(data) {

    rotate(data[1], data[2]);

    aircraftPositionPoints.push({
        x: parseFloat(data[0]),
        y: parseFloat(data[1])
    });

    bankingPoints.push({
        x: parseFloat(data[0]),
        y: parseFloat(data[2])
    });

    chart.render();

}


function drawGraph() {

    chart = new CanvasJS.Chart("div-graph-result", {
        animationEnabled: true,
        title:{
            text: "Lietadlo"
        },
        // axisX: {
        //     valueFormatString: "DD MMM,YY"
        // },
        axisY: {
            // title: "Temperature (in °C)",
            includeZero: false,
            // suffix: " °C"
        },
        legend:{
            cursor: "pointer",
            fontSize: 16,
            // itemclick: toggleDataSeries
        },
        toolTip:{
            shared: true
        },
        data: [{
            name: "Náklon lietadla",
            type: "spline",
            // yValueFormatString: "#0.## °C",
            showInLegend: true,
            dataPoints: aircraftPositionPoints
        },
            {
                name: "Náklon zadnej klapky",
                type: "spline",
                // yValueFormatString: "#0.## °C",
                showInLegend: true,
                dataPoints: bankingPoints
            }]
    });
    chart.render();


}

function clearDatapoints() {

    aircraftPositionPoints.splice(0, aircraftPositionPoints.length);
    bankingPoints.splice(0, bankingPoints.length);
}

function rangeSliderWatcher(sliderValue)
{
    $('#p-slide-range').html(sliderValue);
}

function startCas(file) {

    // sent request for starting CAS
    $.ajax({
        method: "POST",
        url: "./pendulum/cas-start.php",
        data: { file: file },
        error: function (response) {
            alert(response.length);
        }
    });
}

function sendCommandToCAS(command) {

    return sendRequest("../zoli/cas-com.php", "POST", { command : command });

}

function sendRequest(url, method, data) {

    let resp = null;

    $.ajax({
        method: method,
        url: url,
        async: false,
        data: data,
        success: function (response) {
            resp = response;
        },
        error: function (response) {
            resp = response;
        }
    });

    return resp;
}

function isValidJson(str) {

    try {

        JSON.parse(str);

    } catch (e) {

        return false;
    }

    return true;
}

async function drawChartAnim(frequency, data) {

    clearDatapoints();

    for (let i = 0; i < data.length; ++i) {

        createDataPoints(data[i]);

        await sleep(frequency);
    }
}

function sleep(ms) {

    return new Promise(resolve => setTimeout(resolve, ms));
}

function rotate(x, y) {
    let innerArrow = document.getElementById("svg-svg");
    let innerArrow1 = document.getElementById("svg_6");

    let pi = Math.PI;
    x=x * (180/pi);
    y=y * (180/pi);

    innerArrow.style.transform = "rotate("+x+"deg)";
    innerArrow1.style.transformOrigin= "center";
    innerArrow1.style.transform = "rotate("+y+"deg)";
}
