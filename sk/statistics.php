
<?php
include "../Database.php";
include_once "../zoli/zoli-config.php";

$db=new Database(HOST,USERNAME,PASSWORD,DATABASE);

$task1=$db->runQuery("SELECT  page,count(*) as num FROM statistics where page ='task1' group by page");
$task2=$db->runQuery("SELECT  page,count(*) as num FROM statistics where page ='task2' group by page");
$task3=$db->runQuery("SELECT  page,count(*) as num FROM statistics where page ='task3' group by page");
$task4=$db->runQuery("SELECT  page,count(*) as num FROM statistics where page ='task4' group by page");

$dataPoints = array(
    array("y" =>number_format($task1[0]["num"]) , "label" => "Prevrátené kyvadlo" ),
    array("y" => number_format($task2[0]["num"]), "label" => "Gulička na tyči" ),
    array("y" => number_format($task3[0]["num"]), "label" => "Tlmič kolesa" ),
    array("y" => number_format($task4[0]["num"]), "label" => "Náklon lietadla" ),

);

?>


<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <title>Štatistika</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="index.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="../language.js" type="application/javascript"></script>

    <script>
        window.onload = function() {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: "Počet použitia individuálnych úloh"
                },
                axisY: {
                    title: ""
                },
                data: [{
                    type: "column",
                    yValueFormatString: "",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();


            var chart = new CanvasJS.Chart("chartContainer2", {
                animationEnabled: true,
                exportEnabled: true,
                title:{
                    text: "Rozdelenie počtu použitia do koláčového grafu"
                },
                subtitles: [{
                    text: ""
                }],
                data: [{
                    type: "pie",
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabelFontSize: 16,
                    indexLabel: "{label} - #percent%",
                    yValueFormatString: "#,##0",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();

        }
    </script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="nav-menubar">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Domov</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individuálne úlohy
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="inverted_pendulum.php">Inverzné kývadlo</a>
                    <a class="dropdown-item" href="suspention/suspension.php">Tlmič automobilu</a>
                    <a class="dropdown-item" href="ball_and_beam.php">Gulička na tyči</a>
                    <a class="dropdown-item" href="aircraft_pitch.php">Náklon lietadla</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Informácie
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="statistics.php">Štatistika</a>
                    <a class="dropdown-item" href="documentation.php">API Dokumentácia</a>
                    <a class="dropdown-item" href="tasks.php">Rozdelenie úloh</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Jazyk
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">Anglický</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovenský</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
</body>

<div id="cont" style="width: 50%;margin: auto">
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    <br><br>
    <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
</div>
<div style="width: 50%;margin: auto;margin-top:5vw;margin-bottom: 5vw">
    <form class="form-inline" action="send_mail.php" method="post" style="width: fit-content;margin: auto">
        <div class="form-group mx-sm-3 mb-2">
            <label for="inputemail" class="sr-only">Email adress</label>
            <input type="email" class="form-control" name="email" id="inputemail" placeholder="Email adress">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Send mail</button>
    </form>
</div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</html>