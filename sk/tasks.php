<!doctype html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="index.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="../language.js" type="application/javascript"></script>
    <title>Rozdelenie úloh</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Domov</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individuálne úlohy
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="inverted_pendulum.php">Inverzné kývadlo</a>
                    <a class="dropdown-item" href="suspention/suspension.php">Tlmič automobilu</a>
                    <a class="dropdown-item" href="ball_and_beam.php">Gulička na tyči</a>
                    <a class="dropdown-item" href="aircraft_pitch.php">Náklon lietadla</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Informácie
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="statistics.php">Štatistika</a>
                    <a class="dropdown-item" href="documentation.php">API Dokumentácia</a>
                    <a class="dropdown-item" href="tasks.php">Rozdelenie úloh</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Jazyk
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">Anglický</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovenský</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<div id="task-table">
    <h2>Rozdelenie úloh</h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Balázs</th>
        <th scope="col">Mátyás</th>
        <th scope="col">Patrik</th>
        <th scope="col">Zoltán</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">Dvojjazyčnosť</th>
        <td>&#10004;</td>
        <td>&#10004;</td>
        <td>&#10004;</td>
        <td>&#10004;</td>
    </tr>
    <tr>
        <th scope="row">CAS API</th>
        <td></td>
        <td>&#10004;</td>
        <td>&#10004;</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">API kľúč</th>
        <td></td>
        <td></td>
        <td></td>
        <td>&#10004;</td>
    </tr>    <tr>
        <th scope="row">Logovanie</th>
        <td></td>
        <td></td>
        <td>&#10004;</td>
        <td></td>
    </tr>
        <tr>
        <th scope="row">Rozdelenie úloh</th>
        <td>&#10004;</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>  <tr>
        <th scope="row">Dizajn stránok</th>
        <td></td>
        <td></td>
        <td></td>
        <td>&#10004;</td>
    </tr>
    <tr>
        <th scope="row">Štatistika a email</th>
        <td>&#10004;</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">API Dokumentácia</th>
        <td></td>
        <td></td>
        <td>&#10004;</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Exportovanie do PDF a CSV</th>
        <td></td>
        <td>&#10004;</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Inverzné kyvadlo</th>
        <td></td>
        <td></td>
        <td>&#10004;</td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Gulička na tyči</th>
        <td>&#10004;</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Tlmič automobilu</th>
        <td></td>
        <td>&#10004;</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <th scope="row">Náklon lietadla</th>
        <td></td>
        <td></td>
        <td></td>
        <td>&#10004;</td>
    </tr>
    </tbody>
</table>
</div>
</body>
</html>