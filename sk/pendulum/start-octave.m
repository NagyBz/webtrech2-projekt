########################################################################

# load packages
pkg load sockets;
pkg load control;
pkg load io;

# NOTE
# package loading not necessary, if already set in file 'octaverc'

# arg list


# create socket
# s = socket(AF_INET, SOCK_STREAM, 0);
s = socket();

# bind socket to port
if (bind(s, 20296))

    disp("Socket binding failed");

endif

# listen on the specified port
if (listen(s, 1))

    disp("Socket listen failed");

endif

while (true)

    # accept connection to socket from client
    conn = accept(s);

    # receive message from client
    msg = recv(conn, 10000);

    # convert message to string (human readable format)
    msg_str = char(msg);

    # break the while loop on exit
    if (strcmp(msg_str, "exit"))
        break;
    endif

    # try to evaluate
    try

        command_array = strsplit(msg_str, "\n");

        # loop through commands
        for i = 1 : length(command_array)

        # evaluate ech command separately and get the result
        result = eval(command_array{i});

        endfor

        # check if result is numeric or string
        if (isnumeric(result))

            # convert numeric to string and send back result to client
            send(conn, mat2str(result));

        else

            # send back result to client
            send(conn, result);

        endif

    catch

        # error caught
        error_message = lasterror.message;

        # send back error messages
        send(conn, ["ERROR: " error_message]);

    end_try_catch

endwhile

# disconnect the socket
if (!disconnect(s) && !disconnect(conn))

    disp("Socket disconnected");

else

    disp("Socket disconnection failed")

endif

