<?php

require_once 'config-anim-patrik.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pendulum</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="pendulum.css"/>
    <script src="pendulum.js"></script>
</head>
<body>

<input type="range" id="r-range" min="-0.3" max="0.3" step="0.01" value="0.2" oninput="showR(this.value)">
r: <span id="r-range-p">0.2</span>
<br>

<input type="checkbox" id="ckbx-show-graph" checked="checked" onchange="showGraph()">
<label for="ckbx-show-graph">Show Graph</label>
<br>

<input type="checkbox" id="ckbx-show-anim" checked="checked" onchange="showAnim()">
<label for="ckbx-show-anim">Show Animation</label>
<br><br>

<button id="btn-draw-graph" type="button" onclick="draw(this, <?php echo ANIMATION_FREQUENCY; ?>)">
    <img src="start-icon.png" alt="start-icon" width="30" height="30">
</button>

<section id="pendulum-container">

    <div id="div-chart">

        <div id="chart"></div>

    </div>

    <div id="div-anim">

        <canvas id="canvas-pendulum" width="800" height="400"></canvas>

    </div>

</section>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</body>
</html>

