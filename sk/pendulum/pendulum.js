let dataPointsPendulumPosition = [ { x: 0, y: 0 } ];
let dataPointsPendulumAngleDeclination = [ { x: 0, y: 0 } ];
let chart = null;

// pendulum animation
let canvas = null;
let context = null;

let width = 800;
let height = 400;

let g = 6.8;
let L = 150;

let baseX = width / 2;
let baseY = height - 200;
let angle = 0;

$(document).ready(function () {

    drawGraphFromDatapoints('chart');

    canvas = document.getElementById("canvas-pendulum");
    context = canvas.getContext("2d");

    drawPendulum(angle, baseX, baseY);

    startCas();

    preloadParams();
});

function startCas() {

    sendRequest('pendulum/cas-start.php', "POST", { file: "start.m", key: "nvuf/hawoiu/.,pfh9832bv.dfbdf,.32b6" });
}

function onExit() {

    sendRequest('pendulum/cas-com.php', "POST", { command: "exit", key: "nvuf/hawoiu/.,pfh9832bv.dfbdf,.32b6" });
}

window.onbeforeunload = onExit;

function preloadParams() {

    sendRequest('pendulum/cas-com.php', "POST", { command: "preload", key: "nvuf/hawoiu/.,pfh9832bv.dfbdf,.32b6" });
}

function draw(button, frequency) {

    button.disabled = true;

    // request for changing 'r'
    sendRequest('pendulum/cas-com.php', "POST", { command: "r = " + $("#r-range").val(), key: "nvuf/hawoiu/.,pfh9832bv.dfbdf,.32b6" });

    // request for showing graph with new 'r'
    let cmd = "[y,t,x]=lsim(sys,r*ones(size(t)),t,[initPozicia;0;initUhol;0]);\n" + "object2json([y, t])";
    let response = sendRequest('pendulum/cas-com.php', "POST", { command: cmd, key: "nvuf/hawoiu/.,pfh9832bv.dfbdf,.32b6" });

    if (isValidJson(response)) {

        response = JSON.parse(response);
        drawChartAnim(frequency, response).then(() => { button.disabled = false; });
    }
}

async function drawChartAnim(frequency, data) {

    clearDatapoints();

    for (let i = 0; i < data.length; ++i) {

        createDataPoints(data[i]);

        changePendulumPosition(data[i][1] * 1);
        changeBasePosition(data[i][0] * 1000);

        await sleep(frequency);
    }
}

function clearDatapoints() {

    dataPointsPendulumPosition.splice(0, dataPointsPendulumPosition.length);
    dataPointsPendulumAngleDeclination.splice(0, dataPointsPendulumAngleDeclination.length);
}

function sleep(ms) {

    return new Promise(resolve => setTimeout(resolve, ms));
}

function createDataPoints(data) {

    dataPointsPendulumPosition.push({
        x: parseFloat(data[2]),
        y: parseFloat(data[1])
    });

    dataPointsPendulumAngleDeclination.push({
        x: parseFloat(data[2]),
        y: parseFloat(data[0])
    });

    chart.render();
}

function drawGraphFromDatapoints(container) {

    // chart
    chart = new CanvasJS.Chart(container, {
        animationEnabled: true,
        theme: "light1",
        title: {
            text: "Prevrátené kývadlo",
            fontSize: 20,
            fontWeight: "bold"
        },
        legend: {
            cursor: "pointer",
            itemclick: function (e) {
                if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                    e.dataSeries.visible = false;
                } else {
                    e.dataSeries.visible = true;
                }

                e.chart.render();
            },
            fontSize: 18
        },
        axisX: {
            labelFontSize: 17,
            gridThickness: 0.5,
            minimum: 0,
            maximum: 11,
            lineThickness: 5,
            interval: 1,
            tickLength: 10,
            crosshair: {
                enabled: false // true
            }

        },
        axisY: {
            labelFontSize: 17,
            gridThickness: 0.5,
            maximum: 0.35,
            minimum: -0.36,
            lineThickness: 5,
            interval: 0.05,
            tickLength: 10,
            crosshair: {
                enabled: false // true
            }
        },
        data: [
            {
                type: "spline",
                lineThickness: 4,
                lineColor: "blue",
                showInLegend: true,
                legendText: "Pozícia kývadla",
                markerType: "none",
                legendMarkerType: "none",
                dataPoints: dataPointsPendulumPosition
            },
            {
                type: "spline",
                showInLegend: true,
                lineThickness: 4,
                lineColor: "red",
                legendText: "Uhol vychýlenia",
                markerType: "none",
                legendMarkerType: "none",
                dataPoints: dataPointsPendulumAngleDeclination
            }
        ]
    });

    chart.render();
}

function drawPendulum(angle, baseX, baseY) {

    context.clearRect(0, 0, width, height);

    let tipX = baseX + (L * Math.sin(angle));
    let tipY = baseY - L * Math.cos(angle);

    // base
    drawRect(baseX - 30, baseY + 10, 60, 20, '#000000');

    // line
    drawLine(baseX, baseY + 10 + 2, tipX, tipY);

    // floor
    drawLine(0, baseY + 55, width, baseY + 55);

    // head
    drawCircle(tipX, tipY, 15, '#000000');

    // wheels of cart
    drawCircle(baseX - 20, baseY + 42, 10, '#FFFFFF');
    drawCircle(baseX + 20, baseY + 42, 10, '#FFFFFF');

    drawCircle(baseX - 20, baseY + 42, 1, '#FFFFFF');
    drawCircle(baseX + 20, baseY + 42, 1, '#FFFFFF');

}

function drawRect(x, y, w, h, fillStyle) {

    context.beginPath();
    context.lineWidth = 4;
    context.fillStyle = fillStyle;
    context.fillRect(x, y, w, h);
    context.stroke();
}

function drawLine(x1, y1, x2, y2) {

    context.beginPath();
    context.lineWidth = 2;
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
}

function drawCircle(x, y, radius, fillStyle) {

    context.beginPath();
    context.lineWidth = 2;
    context.arc(x, y, radius, 0, 2 * Math.PI);
    context.fillStyle = fillStyle;
    context.fill();
    context.stroke();
}

function resetPendulum() {

    angle = 0;
    baseX = width / 2;
    baseY = height - 150;

    drawPendulum(angle, baseX, baseY);
}

function changePendulumPosition(value) {

    angle = value;

    drawPendulum(angle, baseX, baseY);
}

function changeBasePosition(value) {

    baseX = width / 2 + value;

    drawPendulum(angle, baseX, baseY);
}

function sendRequest(url, method, data) {

    let resp = null;

    $.ajax({
        method: method,
        url: url,
        async: false,
        data: data,
        success: function (response) {
            resp = response;
        },
        error: function (response) {
            resp = response;
        }
    });

    return resp;
}

function isValidJson(str) {

    try {

        JSON.parse(str);

    } catch (e) {

        return false;
    }

    return true;
}

function showR(value) {

    $("#r-range-p").html(value);
}

function showAnim() {

    $("#canvas-pendulum").toggle();
}

function showGraph() {

    $("#chart").toggle();
}








