<?php

include "../../log_request.php";
log_page("task2");

?>

<!DOCTYPE html>
<html lang="sk">
<head>

    <title>Ball and Beam</title>
    <meta charset="utf-8">


    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://d3js.org/d3.v3.min.js"></script>



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../index.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
            integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
            crossorigin="anonymous"></script>
    <script src="../../language.js" type="application/javascript"></script>
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://d3js.org/d3.v3.min.js"></script>


    <style>
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="../index.php">Domov</a>

            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individuálne úlohy
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="../inverted_pendulum.php">Inverzné kývadlo</a>
                    <a class="dropdown-item" href="../suspention/suspension.php">Tlmič automobilu</a>
                    <a class="dropdown-item" href="../ball_and_beam.php">Gulička na tyči</a>
                    <a class="dropdown-item" href="../aircraft_pitch.php">Náklon lietadla</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Informácie
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="../statistics.php">Štatistika</a>
                    <a class="dropdown-item" href="../documentation.php">API Dokumentácia</a>
                    <a class="dropdown-item" href="../tasks.php">Rozdelenie úloh</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Jazyk
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">Anglický</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovenský</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<div class="jumbotron text-center" id="nav">
    <h1>Gulička na tyči - Balázs Nagy</h1>
    <p>Trošku seká.(pre 1 z 5)</p>

</div>
<div>
    <div id="animation" style="width:40vw;float: left">
        <svg width="40vw" height="40vw" style="margin: auto" xmlns="http://www.w3.org/2000/svg"
             xmlns:svg="http://www.w3.org/2000/svg">
            <!-- Created with SVG-edit - http://svg-edit.googlecode.com/ -->
            <g>
                <title>Layer 1</title>
                <rect id="beam" stroke="null"
                      transform="rotate(0.8309829235076904 310.4392089843795,259.62741088866903) " fill="#7f7f7f"
                      stroke-width="5" x="84.5508" y="246.11266" width="450" height="27.02949"/>
                <circle id="ball" r="35.38361" cy="211.10538" cx="310"
                        stroke-dasharray="null" stroke-width="5" stroke="null" fill="#7f7f7f"/>
            </g>
        </svg>

    </div>
    <div style="width: fit-content;float:right;">
        <div id="tester" style="

    width: 50vw;
    height: 15vw;"></div>
        <div id="tester1" style="

    width: 50vw;
    height: 15vw;"></div>
    </div>
</div>


<div class="form-group" style="width:50vw;margin-top: 40vw;margin-left: 25vw">
    <div>
        <button type="button" class="btn btn-primary" onclick="stopp()">Stop</button>
        <button type="button" class="btn btn-primary" onclick="continuee()">Continue</button>

        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" onclick="visual()" value="true"
                   checked="checked">
            <label class="form-check-label" for="inlineCheckbox1">Animacia</label>
        </div>

        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" onclick="visual()" value="true"
                   checked="checked">
            <label class="form-check-label" for="inlineCheckbox2">Grafy</label>
        </div>

    </div>

    <div class="slidecontainer" style="width: fit-content; margin: auto;">
        <input type="range" min="-100" max="100" value="0" style="width: 20vw" class="slider" id="myRange">
        <button type="submit" class="btn btn-primary" style="width: fit-content; margin: auto;" id="myBtn"
                onclick="myFunction(this)">Submit
        </button>
    </div>
    <!--    <input type="number" class="form-control" id="koef" placeholder="Koeficient">-->

</div>


<div id="result"></div>
<div id="cont"></div>

<script src="scripts.js"></script>

</body>