var x=[];
var y1=[];
var y2=[];

var y_showed=true;
var l_showed=true;

var stopped=false;

var margin = {top: 40, right: 80, bottom: 80, left: 80},
    width = 600 - margin.left - margin.right,
    height = 250 - margin.top - margin.bottom;
var xcoord;
var ycoord;

var i=0;



function myFunction(button){


   button.disabled = true;


    if(typeof(EventSource) !== "undefined") {
        let v=document.getElementById("myRange").value/100;
        var source = new EventSource("stream_data.php?key=xnagy123&r="+v);

        TESTER = document.getElementById('tester');
        TESTER1 = document.getElementById('tester1');

        source.addEventListener("message", function(e) {
            var data = JSON.parse(e.data);
            document.getElementById("result").innerHTML = e.data;


            if(vykresliplotpy(data,stopped)==="enough"){source.close();
            console.log("source closed");button.disabled =  false;};

            i++;
            if(i>=498){
                x=[];
                y1=[];
                y2=[];
                button.disabled =  false;
                i=0;
                var domElemnt = document.getElementById("ball");
                if (domElemnt) {
                    var transformAttr = ' translate(' + 0 +',' + 0 + ')';
                    domElemnt.setAttribute('transform', transformAttr);
                }
            }
        }, false);

    } else {
        document.getElementById("result").innerHTML = "Sorry, your  browser does not support server-sent events...";
    }




}



function vykresliplotpy(data,stopped) {

    x.push(data.x);
    y1.push(data.y1);
    y2.push(data.y2);


    y_showed=document.getElementById("inlineCheckbox2").checked;
    l_showed=document.getElementById("inlineCheckbox1").checked;


    if (!stopped){

        Plotly.newPlot( TESTER, [{
                x: x,
                y: y1
            }], {
                margin: { t: 1 },
            }
        );

        Plotly.newPlot( TESTER1, [{
                x: x,
                y: y2
            }], {
                margin: { t: 1 },
            }
        );

        visual();


        moveSection("ball",y2[y2.length-1],y2[y2.length-1]);
        rotatex(y1[y1.length-1]);
    }
    if(x.length>=498){
        return "enough";
    }

}

function moveSection(idStr, xOffset, yOffset) {
    var domElemnt = document.getElementById(idStr);
    if (domElemnt) {
        var transformAttr = ' translate(' + xOffset*220 + ',' + 0 + ')';
        domElemnt.setAttribute('transform', transformAttr);
    }
}

function rotatex(x) {
    var innerArrow = document.getElementById("beam");
    var pi = Math.PI;
    x=x * (180/pi)*100;
    innerArrow.setAttribute("transform", "rotate("+x+")");
}

function stopp() {
    stopped=true;
}
function continuee() {
    stopped=false;
}



function visual() {
    y_showed=document.getElementById("inlineCheckbox2").checked;
    l_showed=document.getElementById("inlineCheckbox1").checked;
    if(y_showed===false){
        document.getElementById("tester").style.visibility="hidden";
        document.getElementById("tester1").style.visibility="hidden";
    }
    if(y_showed===true){
        document.getElementById("tester").style.visibility="visible";
        document.getElementById("tester1").style.visibility="visible";
    }
    if(l_showed===false){
        document.getElementById("animation").style.visibility="hidden";

    }
    if(l_showed===true){
        document.getElementById("animation").style.visibility="visible";

    }

}