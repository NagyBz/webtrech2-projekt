<?php

include_once "../../Cas.php";

if (isset($_POST)) {

    try {

        $file = $_POST["file"];

        Cas::start($file);

    } catch (Exception $e) {

        echo "Error occurred while starting CAS: " . $e->getMessage();
    }
}

?>