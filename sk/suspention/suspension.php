<?php

include "../../log_request.php";
log_page("task3");

?>
<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <title>Tlmič automobilu</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../index.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="../../language.js" type="application/javascript"></script>

    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Tlmič kolesa</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="styleStream.css"/>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbar-dropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="../index.php">Domov</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-tasks" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Individuálne úlohy
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-tasks">
                    <a class="dropdown-item" href="../inverted_pendulum.php">Inverzné kývadlo</a>
                    <a class="dropdown-item" href="suspension.php">Tlmič automobilu</a>
                    <a class="dropdown-item" href="../ball_and_beam.php">Gulička na tyči</a>
                    <a class="dropdown-item" href="../aircraft_pitch.php">Náklon lietadla</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-information" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Informácie
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar-item-information">
                    <a class="dropdown-item" href="../statistics.php">Štatistika</a>
                    <a class="dropdown-item" href="../documentation.php">API Dokumentácia</a>
                    <a class="dropdown-item" href="../tasks.php">Rozdelenie úloh</a>
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbar-item-language" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Jazyk
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-item-language">
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('sk','en')">Anglický</a>
                    <a class="dropdown-item" href="#" onclick="redirectLanguage('en','sk')">Slovenský</a>
                </div>
            </li>
        </ul>
    </div>
</nav>


<section >
    <h1 id="h1">Tlmič kolesa- Radványi Mátyás</h1>
    <div class="slidecontainer">
        <input type="range" step="1" min="-15" max="15" value="0.1" class="slider" id="myRange">
        <p id="val">
            Graf:  <input type="checkbox" id="graf"  onclick="visual()">
            Animácia :  <input type="checkbox" id="animation" onclick="visual()">
            Hodnota r: <span id="demo"> </span>
            <button class="button button1" type="button" onclick="drawGraph()" >Start simulation </button>
        </p>
    </div>



    <div class="container" id="graf_container"  >
        <canvas id="chart"
                style="width: 100%; height: 100%; background: #222; ">
        </canvas>
    </div>

    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <div id="car_svg">
        <svg   width="600" height="600" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
            <!-- Created with SVG-edit - http://svg-edit.googlecode.com/ -->
            <title>Layer 1</title>
            <g>
                <title>Layer 1</title>
                <g stroke="null" transform="rotate(-0.5474498271942139 297.8764648437449,244.22077941894526) " id="svg_1">
                    <path stroke="null" id="svg_8" d="m5.89528,254.28926c1.72057,-194.38338 -3.02553,-216.17603 0.91306,-216.75355c3.93859,-0.57752 228.52655,2.76483 304.09678,13.70355c45.09795,10.10406 81.03801,47.43136 119.17688,75.6251c23.65673,21.20083 51.27422,35.16801 75.06889,55.54504c25.69877,26.07815 49.84233,55.16652 67.65001,89.76015c6.40716,34.5825 25.93321,68.86914 19.4108,103.77683l-11.60612,47.69757c-23.9211,14.91593 -12.16598,8.03866 -62.88759,14.8001c-50.72161,6.7615 -68.22623,-8.70654 -92.06628,-5.99987c-89.88099,-1.61897 -110.52968,19.68811 -157.12051,18.47406c-46.5908,-1.21407 -228.66175,-5.62485 -266.30524,-10.7613" stroke-width="null" fill="#7f0000"/>
                    <path stroke="null" fill="#ffffff" stroke-width="null" stroke-dasharray="null" d="m250.55301,135.33923c0,0 1.28843,164.87109 1.28843,164.87109c0,0 224.18617,60.66011 222.89773,4.66616c-1.28843,-55.99396 -27.37904,-92.93442 -83.10349,-134.54104c-55.72442,-41.60661 -141.08268,-34.99622 -141.08268,-34.99622l0,0.00001z" id="svg_1"/>
                </g>
                <g stroke="null" id="svg_4">
                    <circle stroke="null" fill="#000000" stroke-width="5" stroke-dasharray="null" cx="384.84069" cy="489.99427" r="95.45182" id="svg_3"/>
                    <circle stroke="null" fill="#7f7f7f" stroke-width="5" stroke-dasharray="null" cx="384.91589" cy="490.61149" r="74.34154" id="svg_2"/>
                </g>
            </g>
        </svg>
    </div>
</section>





<script src="com.js"></script>

</body>
</html>