<?php


class CommandLog
{
    private $command;
    private $commandCorrect;
    private $errorMessage;

    public function __construct($command, $commandCorrect, $errorMessage) {

        $this->command = $command;
        $this->commandCorrect = $commandCorrect;
        $this->errorMessage = $errorMessage;
    }

    public function getCommand() {

        return $this->command;
    }

    public function getCommandCorrect() {

        return $this->commandCorrect;
    }

    public function getErrorMessage() {

        return $this->errorMessage;
    }
}
